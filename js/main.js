/*Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
rate (ставка за день роботи), days (кількість відпрацьованих днів).
Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.
*/
class Worker {
    constructor (name, surname, rate, days){
        this.name=name;
        this.surname=surname;
        this.rate=rate;
        this.days=days;
    }
    getSalary(){
       const Salary = this.rate * this.days
       document.getElementById("Salary").innerHTML  += `<p>${this.name} ${this.surname} -  відпрацював: ${this.days} днів, та отримав ${Salary} грн.</p>`;
        // document.write(`<p>${this.name} ${this.surname} відпрацював ${this.days} днів, та отримав ${Salary} грн.</p>`)

    }
}
const worker1 = new Worker ("Петров", "Петро", 50, 20)
const worker2 = new Worker ("Іванов", "Іван", 30, 18)
const worker3 = new Worker ("Назаров", "Микола", 30, 14)
const worker4 = new Worker ("Васильчук", "Василь", 45, 17)
const worker5 = new Worker ("Кучаков", "Сергій", 48, 30)


worker1.getSalary()
worker2.getSalary()
worker3.getSalary()
worker4.getSalary()
worker5.getSalary()

/*
Реалізуйте клас MyString, який матиме такі методи: метод reverse(),
який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(),
який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.
*/

class MyString {
    constructor(parameter){
        this.parameter = parameter;
    }
   reverse() {
    const reverseParameter = this.parameter.split("").reverse().join("");
    document.getElementById("MyStringReverse").innerHTML  += `<p>Початковий рядок: ${this.parameter}</p>`

    document.getElementById("MyStringReverse").innerHTML  += `<p>Перевернутий рядок: ${reverseParameter}</p>`
    document.getElementById("MyStringReverse").innerHTML  += `<hr>`


   }
   ucFirst() {
    const ucFirst = this.parameter[0].toUpperCase()+ this.parameter.slice(1);
    document.getElementById("MyStringUcFirst").innerHTML  += `<p>Початковий рядок: ${this.parameter}</p>`

    document.getElementById("MyStringUcFirst").innerHTML  += `<p>Перевернутий рядок: ${ucFirst}</p>`
    document.getElementById("MyStringUcFirst").innerHTML  += `<hr>`


   }
   ucWords() {
    
    var splits = this.parameter.split(" ");
    var ucWords = "";
  
    for (let i = 0; i < splits.length; i++) {
      let letter = splits[i];
      let toUpperCase = letter.substring(0, 1).toUpperCase();
      let noUpperCase = letter.substring(1, letter.length)
      ucWords += toUpperCase + noUpperCase + " ";
    }
  
    document.getElementById("MyStringUcWords").innerHTML  += `<p>Початковий рядок: ${this.parameter}</p>`

    document.getElementById("MyStringUcWords").innerHTML  += `<p>Перевернутий рядок: ${ucWords}</p>`
    document.getElementById("MyStringUcWords").innerHTML  += `<hr>`


   }
}
const parameter1 = new MyString ("123456")
const parameter2 = new MyString ("я вивчаю JS")
const parameter3 = new MyString ("потрібно завжди вивчати щось нове")


parameter1.reverse()
parameter2.reverse()
parameter3.reverse()

parameter1.ucFirst()
parameter2.ucFirst()
parameter3.ucFirst()

parameter1.ucWords()
parameter2.ucWords()
parameter3.ucWords()

/*
Створіть клас Phone, який містить змінні number, model і weight.
Створіть три екземпляри цього класу.
Виведіть на консоль значення їх змінних.
Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.
*/

class Phone {
    constructor(number, model, weight, name){
        this.number=number;
        this.model=model;
        this.weight=weight;
        this.name=name;
    }
    consol(){
    console.log(`номер: ${this.number}; модель: ${this.model}; вага: ${this.weight}грам`)
    }
    receiveCall(){
        
    console.log(`телефонує ${this.name}`)

    }
    getNumber(){
        return this.number
    }
}
const phone1 = new Phone(+380979760474, "Leeco Le2", 153, "Іван")
const phone2 = new Phone(+380979760000, " Xiaomi Redmi 3", 192, "Петро")
const phone3 = new Phone(+380959830424, "Samsung Galaxy S23", 140, "Василь")

phone1.consol();
phone2.consol();
phone3.consol();
phone1.receiveCall();
phone2.receiveCall();
phone3.receiveCall();

//метод getNumber викликається при необхідності отримати номер.


/*
Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
*/

class Engine{
    constructor(power, company){
        this.power=power;
        this.company=company;
    }
}
class Driver  {
    constructor(fullName, driving){
      
        this.fullName=fullName;
        this.driving=driving;
    }
}



class Car  {
    constructor(carBrand, carClass, weight, driver, engine){
        this.driver=driver
        this.carBrand=carBrand;
        this.carClass=carClass;
        this.weight=weight;
        this.engine=engine
        
    }
    start(){
        console.log("Поїхали")

    }
    stop(){
        console.log("Зупиняємося")

    }
    turnRight(){
        console.log("Поворот праворуч")

    }
    turnLeft(){
        console.log("Поворот ліворуч")

    }
    toString(){
        document.getElementById("Car").innerHTML  += `<p class="line">Водій: ${this.driver.fullName}</p>`
        document.getElementById("Car").innerHTML  += `<p>Стаж водіння: ${this.driver.driving} років</p>`
        document.getElementById("Car").innerHTML  += `<br>`
        document.getElementById("Car").innerHTML  += `<p>Марка авто: ${this.carBrand}</p>`
        document.getElementById("Car").innerHTML  += `<p>Клас авто: ${this.carClass}</p>`
        document.getElementById("Car").innerHTML  += `<p>Вага автомобіля: ${this.weight}kg.</p>`
        document.getElementById("Car").innerHTML  += `<p>Потужність: ${this.engine.power}кВт.</p>`
        document.getElementById("Car").innerHTML  += `<p>Виробник авто: ${this.engine.company}</p>`
    }
    enumeration(){
      
        
          for (let key in this) {
            if(key === "driver"){
                for (let key2 in this.driver) {
                    console.log( key2 +": " +this.driver[key2] )
                    }
            }
            
            else{
                if(key === "engine"){
                    for (let key3 in this.engine) {
                        console.log( key3 +": " +this.engine[key3] )
                        }
                        
                }
                else{
                    console.log( key +": " +this[key] )
                }
                
            }
                    
        }


            console.log(" " )

        
    }
}
const driver1 = new Driver ("Подольський Олексій Сергійович", 6)
const driver2 = new Driver ("Васильчук Василь Васильович ", 2)
const driver3 = new Driver ("Іванов Іван Іванович", 3)

const engine2= new Engine (300, "Електронмаш")
const engine3= new Engine (180, "ВЕПР")
const engine4= new Engine (240, "ЗАЗ")

const car1= new Car("Сенс", "ліфтбек", 1750, {fullName:"Подольський Олексій Сергійович", driving: 6}, {power:180, company:"Електронмаш"})
const car2= new Car("БАЗ А148", "седан", 2890, {fullName:"Васильчук Василь Васильович ", driving:2}, {power:300, company:"ВЕПР"})
const car3= new Car("ВЕПР-М ", "кросовер",3500, {fullName:"Іванов Іван Іванович", driving:3}, {power:240, company:"ЗАЗ"})

car1.start()
car1.stop()
car1.turnRight()
car1.turnLeft()

car2.start()
car2.stop()
car2.turnRight()
car2.turnLeft()

car3.start()
car3.stop()
car3.turnRight()
car3.turnLeft()

car1.toString()
car2.toString()
car3.toString()



class Lorry extends Car {
    constructor(carBrand, carClass, weight, driver, engine,carryingСapacity){
        super(carBrand, carClass, weight, driver, engine);
        this.carryingСapacity=carryingСapacity;
    }
    toString2(){
        super.toString()
        document.getElementById("Car").innerHTML  += `<br>`
        document.getElementById("Car").innerHTML  += `<p>Вантажопідйомність: ${this.carryingСapacity}т.</p>`
        document.getElementById("Car").innerHTML  += `<hr>`

    
    }
}

class SportCar extends Car {
    constructor(carBrand, carClass, weight, driver, engine,speed){
        super(carBrand, carClass, weight, driver, engine);
        this.speed=speed;
    }
    toString3(){
        super.toString()
        document.getElementById("Car").innerHTML  += `<br>`
        document.getElementById("Car").innerHTML  += `<p>Максимальна швидкість ${this.speed}т.</p>`
        document.getElementById("Car").innerHTML  += `<hr>`
    
    }
}
const lorry1= new Lorry ("Сенс", "ліфтбек", 1750, {fullName:"Подольський Олексій Сергійович", driving: 6}, {power:180, company:"Електронмаш"},40)
const lorry2= new Lorry ("БАЗ А148", "седан", 2890, {fullName:"Васильчук Василь Васильович ", driving:2}, {power:300, company:"ВЕПР"},17)
const lorry3= new Lorry ("ВЕПР-М ", "кросовер",3500, {fullName:"Іванов Іван Іванович", driving:3}, {power:240, company:"ЗАЗ"},7)

const sportCar1= new SportCar ("Сенс", "ліфтбек", 1750, {fullName:"Подольський Олексій Сергійович", driving: 6}, {power:180, company:"Електронмаш"},280)
const sportCar2= new SportCar ("БАЗ А148", "седан", 2890, {fullName:"Васильчук Василь Васильович ", driving:2}, {power:300, company:"ВЕПР"},320)
const sportCar3= new SportCar ("ВЕПР-М ", "кросовер",3500, {fullName:"Іванов Іван Іванович", driving:3}, {power:240, company:"ЗАЗ"},240)

lorry1.toString2()
lorry2.toString2()
lorry3.toString2()

sportCar1.toString3()
sportCar1.toString3()
sportCar3.toString3()

//Метод enumeration() - автоматично перебирає усі поля класу.
car1.enumeration()
car2.enumeration()
car3.enumeration()

lorry1.enumeration()
lorry2.enumeration()
lorry3.enumeration()

sportCar1.enumeration()
sportCar1.enumeration()
sportCar3.enumeration()


/*
Створити клас Animal та розширюючі його класи Dog, Cat, Horse.
Клас Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
Dog, Cat, Horse перевизначають методи makeNoise, eat.
Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.
Створіть клас Ветеринар, у якому визначте метод void treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
*/

class Animal {
constructor(food, location){
    this.food=food;
    this.location=location;
}
makeNoise(){
console.log(`${this.food} шумить ${this.location}`)
}
eat(){
    console.log(`${this.food} їсть ${this.location}`)

}
sleep(){
    console.log(`${this.food} спить ${this.location}`)

}
}

class Dog extends Animal{
    constructor(food, location, alias, color){
        super(food, location);
        this.alias=alias;
        this.color=color;
    }
    makeNoise(){
    console.log(`${this.food} проживає за адресою: ${this.location}`)

    }
    eat(){
        console.log(`${this.food} має кличку "${this.alias}"`)
    
    }
}
class Cat extends Animal{
    constructor(food, location, breed, gender){
        super(food, location);
        this.breed=breed;
        this.gender=gender;
    }
    makeNoise(){
        console.log(`${this.food} проживає за адресою: ${this.location}`)
    
    }
    eat(){
            console.log(`${this.food} породи: "${this.breed}"`)
        
    }
}
class Horse extends Animal{
    constructor(food, location, weight, age){
        super(food, location);
        this.weight=weight;
        this.age=age;
    }
    makeNoise(){
        console.log(`${this.food} проживає за адресою: ${this.location}`)
    
    }
    eat(){
            console.log(`${this.food} важить ${this.weight}кг`)
        
    }
}

const animal1=new Animal("Кіт", "кухні")
const animal2=new Animal("Собака", "на дворі")
const animal3=new Animal("Папуга", "в клітці")

animal3.makeNoise()
animal2.eat()
animal1.sleep()

const dog1=new Dog("Собака", "м.Вінниця, вул. Юності, 29", "Оскар", "білий")
const dog2=new Dog("Собака", "м.Дніпро, вул. Мазепи, 12", "Бульдог", "Коричнивий")
const dog3=new Dog("Собака", "м.Одеса, вул. Небесної сотні, 11", "Оскар", "білий")

dog1.makeNoise()
dog2.makeNoise()
dog3.makeNoise()
dog1.eat()
dog2.eat()
dog3.eat()

const cat1=new Cat("Кіт", "м.Вінниця, вул. Юності, 29", "Азійська", "жіноча")
const cat2=new Cat("Кіт", "м.Дніпро, вул. Мазепи, 12", "Американська короткошерста", "чоловіса")
const cat3=new Cat("Кіт", "м.Одеса, вул. Небесної сотні, 11", "Блакитноока кішка", "жіноча")

cat1.makeNoise()
cat2.makeNoise()
cat3.makeNoise()
cat1.eat()
cat2.eat()
cat3.eat()

const horse1=new Horse("Кінь", "м.Вінниця, вул. Юності, 29", 420, 8)
const horse2=new Horse("Кінь", "м.Дніпро, вул. Мазепи, 12", 390, 5)
const horse3=new Horse("Кінь", "м.Одеса, вул. Небесної сотні, 11", 275, 3)

horse1.makeNoise()
horse2.makeNoise()
horse3.makeNoise()
horse1.eat()
horse2.eat()
horse3.eat()

class Veterinarian {
constructor(food, location){
    this.food=food;
    this.location=location
}
treatAnimal(){
    document.getElementById("Animal").innerHTML  += `<p>На прийомі: ${this.food}</p>`
    document.getElementById("Animal").innerHTML  += `<p>Адреса проживання: ${this.location}</p>`
    document.getElementById("Animal").innerHTML  += `<br>`


}
}
const veterinarian1=new Veterinarian("Кіт", "м.Вінниця, вул. Юності, 29")
const veterinarian2=new Veterinarian("Собака", "м.Дніпро, вул. Мазепи, 12")
const veterinarian3=new Veterinarian("Папуга", "м.Одеса, вул. Небесної сотні, 11")

veterinarian1.treatAnimal()
veterinarian2.treatAnimal()
veterinarian3.treatAnimal()

